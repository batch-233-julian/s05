=====================
	-- Activity
=====================


01. -- Return the customerName of the customers who are from the Philippines.
SELECT customerName  FROM customers WHERE country = "Philippines";


02. -- Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts".
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";


03. -- Return the productName and MSRP of the product named "The Titanic".
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";


04. -- Return the firstName and lastName of the employee whose email is "jfirrelli@classicmodelcars.com".
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";


05. -- Return the names of customers who have no registered state.
SELECT customerName FROM customers WHERE state IS NULL;


06. -- Return the firstName, lastName, and email of the employee whose lastName is Patterson and firstName is Steve. 
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";


07. -- Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000.
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;


08. -- Return the customer numbers of orders whose comments contain the string "DHL".
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%"; 


09. -- Return the product lines whose text description mentions the phrase "state of the art".
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";


10. -- Return the countries of customer without duplication.
SELECT DISTINCT country FROM customers ORDER BY country;


11. -- Return the statuses of orders without duplication.
SELECT DISTINCT status FROM orders ORDER BY status;


12. -- Return the customer names and countries of customers whose country is USA, France, or Canada.
SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada") ORDER BY customerName;

13. -- Return the first name, last name, and offices city of employees where their offices are in Tokyo.
SELECT employees.firstName, employees.lastName, offices.city FROM offices JOIN employees ON offices.officeCode = employees.officeCode WHERE offices.city = "Tokyo";


14. -- Return the customer names of customers who where served by the employee named "Leslie Thompson".
SELECT customerName FROM customers JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";


15. Return the product names and customer name of products ordered by "Baane Mini Imports"
SELECT productName, customerName FROM orderdetails JOIN orders ON orderdetails.orderNumber = orders.orderNumber JOIN products ON orderdetails.productCode = products.productCode JOIN customers ON orders.customerNumber = customers.customerNumber  WHERE customers.customerName = "Baane Mini Imports";


16. -- Return employees first names, employees last names, customers names and offices countries of transactions whose customers and offices are the same country
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM customers JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber JOIN offices ON customers.country = offices.country GROUP BY employees.firstName, employees.lastName, customers.customerName, offices.country;


17. -- Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000.
SELECT productName, quantityInStock FROM products WHERE productLine = "planes" AND quantityInStock < 1000;


18. -- Return the customers name with a phone number containing "+81".
SELECT customerName, phone FROM customers WHERE phone LIKE "+81%";




=======================
	-- Stretch Goal
=======================

1. -- Return the product name of the orders where customer name is Baane Mini Imports
SELECT productName FROM orderdetails JOIN orders ON orderdetails.orderNumber = orders.orderNumber JOIN products ON orderdetails.productCode = products.productCode JOIN customers ON orders.customerNumber = customers.customerNumber  WHERE customers.customerName = "Baane Mini Imports";


2. --  Return the last name and first name of employees that reports to Anthony Bow
SELECT lastName, firstName FROM employees WHERE reportsTo = (SELECT employeeNumber FROM employees WHERE firstName = "Anthony" AND lastName = "Bow");


3. -- Return the product name of the product with the maximum MSRP
SELECT productName FROM products ORDER BY MSRP DESC LIMIT 1;


4. -- Return the number of products group by productline
SELECT COUNT(productName), productLine FROM products GROUP BY productLine;


5. -- Return the number of producs where the status is cancelled
SELECT COUNT(orderNumber) FROM orders WHERE status = "cancelled";

    	








